class AddUsersMoviesAndSeasonsToPurchases < ActiveRecord::Migration[6.0]
  def change
  	add_reference :purchases, :user
  	add_reference :purchases, :movie
  	add_reference :purchases, :season
  end
end
