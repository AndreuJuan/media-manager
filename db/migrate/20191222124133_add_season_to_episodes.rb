class AddSeasonToEpisodes < ActiveRecord::Migration[6.0]
  def change
    add_reference :episodes, :season, index: true
  end
end
