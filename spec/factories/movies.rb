FactoryBot.define do
  factory :movie do
    title { FFaker::Movie.unique.title }
    plot { FFaker::Lorem.paragraph(paragraph_count=3) }
  end
end