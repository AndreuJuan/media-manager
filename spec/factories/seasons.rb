FactoryBot.define do
  factory :season do
    title { FFaker::Lorem.sentence(word_count = 3) }
    plot { FFaker::Lorem.paragraph(paragraph_count = 3) }
    number { 0 }
  end
end
