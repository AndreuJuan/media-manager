FactoryBot.define do
  factory :purchase do
    price { rand(0.2...99.9) }
    video_quality { (rand(1..2)%2)==1 ? "HD" : "SD" }
  end
end
