require 'rails_helper'


describe ApiController, type: :controller do 
  render_views

  describe "GET /api/movie_list" do
    before do
      @created_movies = generate_movies
      get :movie_list
    end

    it "returns http success" do
      expect(response).to have_http_status(:ok)
    end

    it "JSON response contains the correct movie attributes" do
      json_response = JSON.parse(response.body)
      expect_matching_attributes(json_response[0], @created_movies[0].attributes)
    end

    it "returns the correct number of results" do
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(@created_movies.size)
    end

    it "JSON response matches all the created movies in order" do
      json_response = JSON.parse(response.body)
      expect(json_response.to_json).to match(@created_movies.to_json)
    end
  end


  describe "GET /api/season_list" do
    before do
      @seasons = generate_seasons_and_episodes
      @episode = @seasons[0].episodes[0]
      @season_expected_attributes = @seasons[0].attributes
      @season_expected_attributes["episodes"] = ""
      get :season_list
    end

    it "returns http success" do
      expect(response).to have_http_status(:ok)
    end

    it "JSON response contains the correct season attributes" do
      json_response = JSON.parse(response.body)
      expect_matching_attributes(json_response[0], @season_expected_attributes)
      expect_matching_attributes(json_response[0]["episodes"][0], @episode.attributes)
    end

    it "returns the correct number of results" do
      json_response = JSON.parse(response.body)
      expect(json_response.size).to eq(@seasons.size)
      counter = 0
      @seasons.each do |season|
        expect(json_response[counter]["episodes"].size).to eq(season.episodes.size)
        counter += 1
      end
    end

    it "JSON response matches all the created seasons and episodes in order" do
      json_response = JSON.parse(response.body)
      counter = 0
      @seasons.each do |season|
        complete_season = season.attributes
        complete_season[:episodes] = season.episodes
        expect(json_response[counter].to_json).to match(complete_season.to_json)
        counter += 1
      end
    end
  end


  describe "GET /api/movie_and_season_list" do
    before do
      @created_movies = generate_movies
      @seasons = generate_seasons_and_episodes
      @episode = @seasons[0].episodes[0]
      @season_expected_attributes = @seasons[0].attributes
      @season_expected_attributes["episodes"] = ""
      get :movie_and_season_list
    end

    it "returns http success" do
      expect(response).to have_http_status(:ok)
    end

    it "JSON response contains the correct attributes" do
      json_response = JSON.parse(response.body)
      expect_matching_attributes(json_response, {"movies" => "", "seasons" => ""})
      expect_matching_attributes(json_response["movies"][0], @created_movies[0].attributes) #Check the movies have the correct attributes
      expect_matching_attributes(json_response["seasons"][0], @season_expected_attributes) #Check the seasons have the correct attributes
      expect_matching_attributes(json_response["seasons"][0]["episodes"][0], @episode.attributes) #Check the episodes have the correct attributes
    end

    it "returns the correct number of results" do
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq(2)
      expect(json_response["movies"].size).to eq(@created_movies.size)
      expect(json_response["seasons"].size).to eq(@seasons.size)
      counter = 0
      @seasons.each do |season|
        expect(json_response["seasons"][counter]["episodes"].size).to eq(season.episodes.size)
        counter += 1
      end
    end

    it "JSON response matches all the created movies, seasons and episodes in order" do
      json_response = JSON.parse(response.body)
      expect(json_response["movies"].to_json).to match(@created_movies.to_json)
      counter = 0
      @seasons.each do |season|
        complete_season = season.attributes
        complete_season[:episodes] = season.episodes
        expect(json_response["seasons"][counter].to_json).to match(complete_season.to_json)
        counter += 1
      end
    end
  end

  describe "POST /api/purchase_content" do

    context "when some parameter is missing" do
      before(:each) do
        #The user is not included
        post :purchase_content, params: { content_id: "132", content_type: "movie", price: 2.99, video_quality: "HD" }
      end

      it "renders an error json" do
        expect_receive_error(response, "Missing expected parameters")
      end
    end

    context "when the user does not exist" do
      before(:each) do
        #The user is not included
        post :purchase_content, params: {user_email: "unexisting_user@email.com", content_id: "132", content_type: "movie", price: 2.99, video_quality: "HD" }
      end

      it "renders an error json" do
        expect_receive_error(response, "User not found")
      end
    end

    context "when an incorrect content type is sent" do
      before(:each) do
        @user = FactoryBot.create(:user)
        #The content type is incorrect
        post :purchase_content, params: {user_email: @user.email, content_id: "132", content_type: "wrong", price: 2.99, video_quality: "HD" }
      end

      it "renders an error json" do
        expect_receive_error(response, "Incorrect content type")
      end
    end

    context "when a user tries to purchase a content that was recently purchased" do
      before(:each) do
        user = FactoryBot.create(:user)
        movies = generate_movies
        seasons = generate_seasons_and_episodes

        # Randomly select a season or a movie
        if (rand(1..2) % 2) == 1
          # Select a movie
          # Parameters for the factory
          movie_id = movies.sample.id
          season_id = nil

          # Parameters for the post
          content_id = movie_id
          content_type = "movie"
        else
          # Select a season
          # Parameters for the factory
          movie_id = nil
          season_id = seasons.sample.id

          # Parameters for the post
          content_id = season_id
          content_type = "season"
        end

        # Customize the creation time so it is between 2 days ago and today
        creation_time = FFaker::Time.between(Time.now-2.day, Time.now)
        purchase = FactoryBot.create(:purchase, user_id: user.id, movie_id: movie_id, season_id: season_id, created_at: creation_time, updated_at: creation_time)

        post :purchase_content, params: {user_email: user.email, content_id: content_id, content_type: content_type, price: 2.99, video_quality: "HD" }
      end

      it "renders an error json" do
        expect_receive_error(response, "The user purchased this content recently")
      end
    end

    context "when a user tries to purchase a content with an incorrect parameter" do
      before(:each) do
        user = FactoryBot.create(:user)
        movies = generate_movies
        seasons = generate_seasons_and_episodes

        # Randomly select a season or a movie
        if (rand(1..2) % 2) == 1
          # Select a movie
          # Parameters for the factory
          movie_id = movies.sample.id
          season_id = nil

          # Parameters for the post
          content_id = movie_id
          content_type = "movie"
        else
          # Select a season
          # Parameters for the factory
          movie_id = nil
          season_id = seasons.sample.id

          # Parameters for the post
          content_id = season_id
          content_type = "season"
        end
      
        video_quality = (rand(1..2)%2)==1 ? "HD" : "SD"
        #The content type is incorrect
        post :purchase_content, params: {user_email: user.email, content_id: content_id, content_type: content_type, price: "incorrect_parameter", video_quality: video_quality }
      end

      it "renders an error json" do
        expect_receive_error(response, "The price has not a correct format")
      end
    end

    context "when a user purchases a content successfully" do
      before(:each) do
        @user = FactoryBot.create(:user)
        movies = generate_movies
        seasons = generate_seasons_and_episodes

        # Randomly select a season or a movie
        if (rand(1..2) % 2) == 1
          # Select a movie
          # Parameters for the factory
          @movie_id = movies.sample.id
          @season_id = nil

          # Parameters for the post
          content_id = @movie_id
          content_type = "movie"
        else
          # Select a season
          # Parameters for the factory
          @movie_id = nil
          @season_id = seasons.sample.id

          # Parameters for the post
          content_id = @season_id
          content_type = "season"
        end

        @price = rand(0.2...99.9)
        @price = @price.round(2)
        @video_quality = (rand(1..2)%2)==1 ? "HD" : "SD"
        #The content type is incorrect
        post :purchase_content, params: {user_email: @user.email, content_id: content_id, content_type: content_type, price: @price, video_quality: @video_quality }
      end

      it 'returns the created purchase' do
        expect(JSON.parse(response.body)['price'].to_f).to eq(@price)
        expect(JSON.parse(response.body)['video_quality']).to eq(@video_quality)
        expect(JSON.parse(response.body)['user_id']).to eq(@user.id)
        expect(JSON.parse(response.body)['season_id']).to eq(@season_id)
        expect(JSON.parse(response.body)['movie_id']).to eq(@movie_id)
      end
    end
  end

  describe "GET /api/user_library" do

    context "when some parameter is missing" do
      before(:each) do
        #No parameters
        get :user_library, params: {  }
      end

      it "renders an error json" do
        expect_receive_error(response, "Missing expected parameters")
      end
    end

    context "when the user does not exist" do
      before(:each) do
        #A user that does not exist
        post :user_library, params: {user_email: "unexisting_user@email.com" }
      end

      it "renders an error json" do
        expect_receive_error(response, "User not found")
      end
    end

    context "retrieve the user library from a existing user ordered by the remaining time to watch the content" do
      before(:each) do
        user = FactoryBot.create(:user)
        movies = generate_movies
        seasons = generate_seasons_and_episodes

        @season_expected_attributes = seasons[0].attributes
        @season_expected_attributes["episodes"] = ""

        @recent_movies = []
        @recent_seasons = []
        rand(6..16).times do
          # Randomly select a season or a movie
          if (rand(1..2) % 2) == 1
            # Select a movie
            # Parameters for the factory
            current_movie = movies.sample
            #Required to avoid repeated values
            movies.delete(current_movie)
            movie_id = current_movie.id
            season_id = nil

            # Parameters for the post
            content_id = movie_id
            content_type = "movie"
          else
            # Select a season
            # Parameters for the factory
            current_season = seasons.sample
            seasons.delete(current_season)
            movie_id = nil
            season_id = current_season.id

            # Parameters for the post
            content_id = season_id
            content_type = "season"
          end

          if (rand(1..2) % 2) == 1
            # Customize the creation time so it is between 2 days ago and today
            creation_time = FFaker::Time.between(Time.now-2.day, Time.now)
            if current_movie
              movie_data = {movie: current_movie, purchase_time: creation_time}
              @recent_movies << movie_data
            else
              season_data = {season: current_season, purchase_time: creation_time}
              @recent_seasons << season_data
            end
          else
            # Customize the creation time so it is between 1 year ago and 2 days ago
            creation_time = FFaker::Time.between(Time.now-1.year, Time.now-2.day)
          end
          purchase = FactoryBot.create(:purchase, user_id: user.id, movie_id: movie_id, season_id: season_id, created_at: creation_time, updated_at: creation_time)
        end

        # Sort the purchased movies in ascending order
        @recent_movies.sort_by! { |purchases| purchases[:purchase_time] }
        # Sort the purchased seasons in ascending order
        @recent_seasons.sort_by! { |purchases| purchases[:purchase_time] }

        get :user_library, params: { user_email: user.email }
      end

      it "returns http success" do
        expect(response).to have_http_status(:ok)
      end

      it "JSON response contains the correct attributes" do
        json_response = JSON.parse(response.body)
        expect_matching_attributes(json_response, {"movies" => "", "seasons" => ""})
        if @recent_movies.size > 0
          expect_matching_attributes(json_response["movies"][0], @recent_movies[0][:movie].attributes) #Check the movies have the correct attributes
        end
        if @recent_seasons.size > 0
          expect_matching_attributes(json_response["seasons"][0], @season_expected_attributes) #Check the seasons have the correct attributes
          expect_matching_attributes(json_response["seasons"][0]["episodes"][0], @recent_seasons[0][:season].episodes[0].attributes) #Check the episodes have the correct attributes
        end
      end

      it "returns the correct number of results" do
        json_response = JSON.parse(response.body)
        expect(json_response.count).to eq(2)
        expect(json_response["movies"].size).to eq(@recent_movies.size)
        expect(json_response["seasons"].size).to eq(@recent_seasons.size)
        counter = 0
        @recent_seasons.each do |season|
          expect(json_response["seasons"][counter]["episodes"].size).to eq(season[:season].episodes.size)
          counter += 1
        end
      end

      it "JSON response matches all the created movies, seasons and episodes from the user library ordered by the remaining time to watch the content" do       
        json_response = JSON.parse(response.body)
        counter = 0
        json_response["movies"].each do |movie|
          expect(movie.to_json).to match(@recent_movies[counter][:movie].to_json)
          counter += 1
        end
        counter = 0
        json_response["seasons"].each do |season|
          complete_season = @recent_seasons[counter][:season].attributes
          complete_season[:episodes] = @recent_seasons[counter][:season].episodes
          expect(season.to_json).to match(complete_season.to_json)
          counter += 1
        end
      end
    end
  end

  # DRY methods

  def generate_movies
    # Generate random movies with different titles for testing
    created_movies = []
    rand(16..24).times do
      created_movies << FactoryBot.create(:movie)
    end
    # Order the array by descending creation order
    created_movies = created_movies.reverse
    return created_movies
  end

  def generate_seasons_and_episodes
    seasons = []
    season_counter = 1
    rand(16..24).times do
      current_season = FactoryBot.create(:season, number: season_counter)
      episode_counter = 1
      rand(1..12).times do
        episode = FactoryBot.create(:episode, number: episode_counter, season_id: current_season.id)
        episode_counter += 1
      end
      seasons << current_season
      season_counter += 1
    end
    # Order the array by descending creation order
    seasons = seasons.reverse
    return seasons
  end

  def expect_matching_attributes(received_element, created_element)
    expect(received_element.keys).to match_array(created_element.keys)
  end

  def expect_receive_error(received_response, message)
    json_response = JSON.parse(received_response.body)
    expect(received_response).to have_http_status(:bad_request)
    expect(json_response).to have_key("error")
    expect(json_response["message"]).to eq(message)
  end

end