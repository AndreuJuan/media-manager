class Movie < ApplicationRecord

	has_many :pruchases

	def self.ordered_data
		Movie.order(created_at: :desc)
	end
	
end
