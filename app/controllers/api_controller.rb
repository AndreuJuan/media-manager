class ApiController < ApplicationController

	def movie_list
		render json: Movie.ordered_data
	end

	def season_list
		render json: seasons_with_episodes
	end

	def movie_and_season_list
		response = {}
		response[:movies] = Movie.ordered_data
		response[:seasons] = seasons_with_episodes
		render json: response
	end

	def purchase_content
		unless params[:user_email] and params[:content_id] and params[:content_type] and params[:price] and params[:video_quality]
			render json: {error: "error", message: "Missing expected parameters"}, status: :bad_request
			return
		end

		user = User.find_by_email params[:user_email]
		unless user
			render json: {error: "error", message: "User not found"}, status: :bad_request
			return
		end

		movie = nil
		season = nil
		case params[:content_type]
		when 'season'
			season = Season.find_by_id params[:content_id]
		when 'movie'
			movie = Movie.find_by_id params[:content_id]
		else
			render json: {error: "error", message: "Incorrect content type"}, status: :bad_request
			return
		end

		season_id = season ? season.id : nil
		movie_id = movie ? movie.id : nil

		price = Float(params[:price]) rescue false
		unless price
			render json: {error: "error", message: "The price has not a correct format"}, status: :bad_request
			return
		end

		# Check that the user has no purchase of the same content for the previous 2 days
		if !Purchase.where(user_id: user.id, season_id: season_id, movie_id: movie_id).where("created_at >= ?", Time.now-2.day).exists?
			purchase = Purchase.create(price: params[:price], video_quality: params[:video_quality], user_id:user.id, season_id: season_id, movie_id: movie_id)
			render json: purchase
			return
		else
			render json: {error: "error", message: "The user purchased this content recently"}, status: :bad_request
			return
		end
	end

	def user_library
		unless params[:user_email]
			render json: {error: "error", message: "Missing expected parameters"}, status: :bad_request
			return
		end

		user = User.find_by_email params[:user_email]
		unless user
			render json: {error: "error", message: "User not found"}, status: :bad_request
			return
		end

		purchases = Purchase.where(user_id: user.id).where("created_at >= ?", Time.now-2.day).order(created_at: :asc)

		season_list = []
		movie_list = []

		purchases.each do |purchase|
			if season = purchase.season
				season_list << single_season_with_episodes(season)
			else
				movie_list << purchase.movie
			end
		end

		response = {}
		response[:movies] = movie_list
		response[:seasons] = season_list
		render json: response
	end


	private

	def seasons_with_episodes
		seasons = Season.order(created_at: :desc)

		response = []
		seasons.each do |season|
			response << single_season_with_episodes(season)
		end

		return response
	end

	def single_season_with_episodes(season)
		season_data = season.attributes
		season_data[:episodes] = season.episodes.order(number: :asc)
		return season_data
	end

end
