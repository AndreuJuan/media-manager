Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
  	get 'movie_list'
  	get 'season_list'
  	get 'movie_and_season_list'
  	post 'purchase_content'
  	get 'user_library'
  end
end
